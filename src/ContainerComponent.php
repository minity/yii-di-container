<?php
/*
 * This file is part of the minity/yii-di-container package.
 *
 * (c) Anton Tyutin <anton@tyutin.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Minity\DependencyInjection;

use CApplicationComponent;

abstract class ContainerComponent extends CApplicationComponent implements ContainerInterface
{
    /**
     * @var string|string[]
     */
    public $load = array();

    /**
     * @throws ConfigurationLoadException if can't load configuration
     */
    public function init()
    {
        foreach ((array)$this->load as $filename) {
            $this->load($filename);
        }
    }
}
