<?php
/*
 * This file is part of the minity/yii-di-container package.
 *
 * (c) Anton Tyutin <anton@tyutin.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Minity\DependencyInjection;

class ConfigurationLoadException extends ContainerException
{

}
