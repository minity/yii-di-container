<?php
/*
 * This file is part of the minity/yii-di-container package.
 *
 * (c) Anton Tyutin <anton@tyutin.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Minity\DependencyInjection;

interface ContainerInterface
{
    /**
     * Get service by name
     *
     * @param string $serviceName
     *
     * @return object
     */
    public function get($serviceName);

    /**
     * Check service by name
     *
     * @param string $serviceName
     *
     * @return
     */
    public function has($serviceName);

    /**
     * Get container parameter by name
     *
     * @param string $name
     *
     * @return mixed
     */
    public function getParameter($name);

    /**
     * Set container parameter by name
     *
     * @param string $name
     * @param mixed  $value
     */
    public function setParameter($name, $value);

    /**
     * Load configuration from file into container
     *
     * @param string $filename
     *
     * @throws ConfigurationLoadException if can't load configuration
     */
    public function load($filename);
}
