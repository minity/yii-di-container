<?php
/*
 * This file is part of the minity/yii-di-container package.
 *
 * (c) Anton Tyutin <anton@tyutin.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Minity\DependencyInjection;

/**
 * Getting components from DI container.
 *
 * Use this trait with module or application class.
 */
trait ContainerComponentsTrait
{
    /**
     * Retrieve component from DIC or default component definition otherwise.
     *
     * @param string $componentName
     * @param bool $createIfNull
     *
     * @return object|null
     */
    public function getComponent($componentName, $createIfNull = true)
    {
        return $this->hasService($componentName)
            ? $this->getService($componentName)
            : parent::getComponent($componentName, $createIfNull);

    }

    /**
     * Check component exists in DIC or defined by application configuration
     *
     * @param string $componentName
     *
     * @return bool
     */
    public function hasComponent($componentName)
    {
        return $this->hasService($componentName) || parent::hasComponent($componentName);
    }

    /**
     * Check component exists in DIC
     *
     * @param string $name
     *
     * @return bool
     */
    public function hasService($name)
    {
        return ($container = $this->getContainer()) ? $container->has($name) : false;
    }

    /**
     * Get component from DIC
     *
     * @param string $name
     *
     * @return object|null
     */
    public function getService($name)
    {
        return ($container = $this->getContainer()) && $container->has($name) ? $container->get($name) : null;
    }
}
