<?php
/*
 * This file is part of the minity/yii-di-container package.
 *
 * (c) Anton Tyutin <anton@tyutin.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Minity\DependencyInjection\Test;

use Minity\DependencyInjection\ContainerAwareInterface;
use Minity\DependencyInjection\ContainerComponentsTrait;
use Minity\DependencyInjection\ContainerInterface;
use PHPUnit_Framework_TestCase;

class ComponentsTraitTest extends PHPUnit_Framework_TestCase
{
    /** @var ApplicationStub */
    static public $app;
    /** @var \PHPUnit_Framework_MockObject_MockObject|\Minity\DependencyInjection\ContainerComponent */
    public $container;

    public static function setUpBeforeClass()
    {
        self::$app = new ApplicationStub(array(
            'basePath' => __DIR__,
            'components' => array(
                'comp1' => array('class' => 'Minity\DependencyInjection\Test\ComponentStub', 'name' => 'comp1'),
                'comp2' => array('class' => 'Minity\DependencyInjection\Test\ComponentStub', 'name' => 'comp2'),
            )
        ));
    }

    /**
     * @return array
     */
    public function setUp()
    {
        $this->container = $this->getMockForAbstractClass('Minity\DependencyInjection\ContainerComponent');
        self::$app->setComponent('container', $this->container);
    }

    public function testGetComponent_StandardComponent()
    {
        $this->container->expects($this->any())->method('has')->willReturn(false);

        $this->assertInstanceOf('Minity\DependencyInjection\Test\ComponentStub', self::$app->getComponent('comp1'));
        $this->assertEquals('comp2', self::$app->getComponent('comp2')->name);
    }

    public function testGetComponent_ServiceComponent()
    {
        $service = new ComponentStub('comp3');
        $this->container->expects($this->any())->method('has')->with('comp3')->willReturn(true);
        $this->container->expects($this->once())->method('get')->with('comp3')->willReturn($service);
        $this->assertSame($service, self::$app->getComponent('comp3'));
    }

    public function testGetComponent_ServiceOverrideStandardComponent()
    {
        $service = new ComponentStub('comp3');
        $this->container->expects($this->any())->method('has')->willReturnMap(array(
            array('comp1', true)
            // else false
        ));
        $this->container->expects($this->once())->method('get')->with('comp1')->willReturn($service);
        $this->assertEquals('comp3', self::$app->getComponent('comp1')->name);
        $this->assertEquals('comp2', self::$app->getComponent('comp2')->name);
    }

    public function testHasService()
    {
        $this->container->expects($this->any())->method('has')->willReturnMap(array(
            array('comp1', true),
            array('comp2', false)
        ));
        $this->assertTrue(self::$app->hasService('comp1'));
        $this->assertFalse(self::$app->hasService('comp2'));
    }

    public function testGetService()
    {
        $service = new ComponentStub('comp1');
        $this->container->expects($this->any())->method('has')->willReturnMap(array(
            array('comp1', true)
            // else false
        ));
        $this->container->expects($this->any())->method('get')->with('comp1')->willReturn($service);
        $this->assertEquals('comp1', self::$app->getService('comp1')->name);
        $this->assertNull(self::$app->getService('comp2'));
    }
}

class ComponentStub extends \CApplicationComponent
{
    public $name;

    public function __construct($name = null)
    {
        $this->name = $name;
    }

}

class ApplicationStub extends \CWebApplication implements ContainerAwareInterface
{
    use ContainerComponentsTrait;

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return parent::getComponent('container');
    }
}
